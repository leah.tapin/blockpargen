#!/usr/bin/env python3

# blockpargen
# authors: Kévin Perrot, Léah Tapin, Sylvain Sené
# date: 2023

# This code implements the three enumeration algorithms presented in the article
# "On countings and enumerations of block-parallel automata networks"
# by Kévin Perrot, Sylvain Sené and Léah Tapin,
# available at https://arxiv.org/abs/2304.09664.

# LICENCE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# Block-parallel update modes enumerator, in the context of automata networks of size n:
# either BPn (all), BPn0 (up to identical dynamics), BPn* (up to isomorphic limit-dynamics).
# Example: python3 blockpargen.py 5 all -f p_order

# This code implements three algorithms in one.
# The algorithm implemented is chosen with the argument "mode",
# which can take one of the three following values: 

# this is python, the main is at the end of this file.

from itertools import combinations, permutations, repeat
from math import lcm, gcd
from argparse import ArgumentParser
from time import time

ALL = 0  	# Algorithm 1
EQUIV = 1	# Algorithm 2
ISO = 2		# Algorithm 3

PORDERS = 0
SBLOCKS = 1
BOTH = 2

# parse argument

parser = ArgumentParser()
parser.add_argument("n", type=int, help="number of automata")
parser.add_argument("mode", choices=['all', 'eq0', 'iso'], help="update mode: 'all' for BPn, 'eq0' for BPn0, 'iso' for BPn*.")
parser.add_argument("-f", "--format", choices=['p_order', 'seq_blocks', 'both'], help="output format: 'p_order' for partitioned orders, 'seq_blocks' for sequences of blocks, 'both' for partitioned orders and sequences of blocks (in that order).", default='p_order')
parser.add_argument("--BPonly", help="generate only update modes that are not block-sequential.", action="store_true")

args = parser.parse_args()

n = args.n
mode = ['all', 'eq0', 'iso'].index(args.mode) # convert mode to integer
with_bs = not args.BPonly
output = ['p_order', 'seq_blocks', 'both'].index(args.format) # convert output to integer

# enumerate the partitions of n
def partitions(n):
    """
    generator of every partition of n (each partition is a list in increasing order)
    source: https://jeromekelleher.net/generating-integer-partitions.html
    """
    a = [0 for i in range(n + 1)]
    k = 1
    y = n - 1
    while k != 0:
        x = a[k - 1] + 1
        k -= 1
        while 2 * x <= y:
            a[k] = x
            y -= x
            k += 1
        l = k + 1
        while x <= y:
            a[k] = x
            a[l] = y
            yield a[:k + 2]
            x += 1
            y -= 1
        a[k] = x + y
        y = x + y - 1
        yield a[:k + 1]

# iterator on the family of block-parallel:
# foreach in ite(n) iterates over block-parallel on n automata
# (as lists of lists : sequence of blocks)
def ite(n, mode, output, with_bs=True):
	"""
	EnumBP(eq0)(iso)
	generates block-parallel update modes of size n from the subset of BPn defined by mode 
	and with_bs, both in their partitioned order form and their sequence of blocks form
	"""
	for partition in partitions(n):
		# Iterates every partition (the sum in each formula)
		if partition[0] != partition[-1] or with_bs:
			# verify that the updating mode isn't also a block-sequential one, which
			# happens iff every number of the partition is the same
			
			# Get the multiplicity of each element of the partition
			multis = [0 for i in range(n+1)]
			for j in partition:
				multis[j] += 1  
			
			# Get the a_j coefficients for algorithm 3
			block_min = [i for i in range(n+1)]
			if mode == ISO:
				facteur = 1
				for j in range(n, 0, -1):
					if multis[j] > 0:
						block_min[j] = gcd(facteur, j)
						facteur = lcm(facteur, j)
						
			# Generate every updating mode required		
			for bp in ite_rec(partition, mode, 1, [i for i in range(n)], multis, block_min):
				# ite_rec generates partitioned orders, when sometimes the sequence of blocks form is needed
				res = []
				if output > 0: # Only compute the sequence of blocks form when needed
					# calculate p(n)
					if len(bp) == 1:
						p_n = len(bp[0])
					else:
						p_n = lcm(len(bp[1]), len(bp[0]))
						for i in range(2, len(bp)):
							p_n = lcm(p_n, len(bp[i]))
					# create each block
					for j in range(p_n):
						new_block = []
						for block in bp:
							new_block += [block[j % len(block)]]
						res += [new_block]
				yield bp, res

# recursive part of the iterator
def ite_rec(partition, mode, j, left_to_choose, multis, block_min):
	"""
	EnumBP(eq0)(iso)aux
	partition : the 'shape' of the blocks of the block-parallel updating mode
	j : the length of the blocks that will be generated during this use of the
	function
	left_to_choose : list of the automata that haven't been put in a block yet 
	"""
	if partition[-1] < j:
		# terminal case : there are no more blocks left to fill
		yield []
	else:
		if multis[j] == 0:
			# j is not in the partition generating this updating mode, so we jump
			# straight to the recursive call
			for bp in ite_rec(partition, mode, j+1, left_to_choose, multis, block_min):
				yield bp
		else:
			for content in combinations(left_to_choose, j*multis[j]): # choose the
			# automata that will go into the matrix of size m*j
			
				# remove those m*j automata from left_to_choose
				newlist = left_to_choose.copy()
				for number in content:
					newlist.remove(number)
				
				# Algorithm 1
				if mode == ALL:
					for first_column in combinations(content, multis[j]):
						# fix the content of the first column and remove 
						# said content of the pool from which to choose
						other = list(content)
						for elt in first_column:
							other.remove(elt)
						
						# The list is already sorted, we only need to iterate the 
						# permutations of the content of the rest of the matrix
						for version in permutations(other):
							blocks = [[first_column[i]]for i in range(multis[j])]
							for i in range(len(version)):
								blocks[i%multis[j]] += [version[i]]
							# recursive call
							for bp in ite_rec(partition, mode, j+1, newlist, multis, block_min):
								yield blocks + bp 
				
				# Algorithms 2 and 3
				else:
					m_j = content[0]				
					for blocks in arrangements(list(content), mode, j, multis[j], m_j, block_min[j]): # iterate
					# all the possible arrangements for the m*j automata
						# recursive call
						for bp in ite_rec(partition, mode, j+1, newlist, multis, block_min):
							yield blocks + bp

def arrangements(content, mode, j, multi, m_j, a_j):
	"""
	EnumBlock(eq0/iso)
	generates all the arrangements leading to different update modes for the
	automata in content in multi (m) blocks of length j
	m_j is the automata with the smallest number and a_j the furthest column
	in which it can be placed (only used by Algorithm 3)
	"""
	stop_val = multi*(j+1-a_j)
	for blocks in arrangements_rec(content, mode, j, multi, stop_val, m_j):
		# arrangements_rec returns the blocks as a list of j lists of size m, where
		# each list is made of all the automata updated at the same time. It must be
		# transposed to obtain the list of m blocks, each of size j
		res = []
		for i in range(multi):
			inter = []
			for k in range(j):
				inter += [blocks[k][i]]
			res += [inter]
		yield res

def arrangements_rec(content, mode, j, multi, stop_val, m_j):
	"""
	EnumBlock(eq0/Iso)Aux
	very similar to ite_rec in its functioning, except that each use of this
	function generates the m automata that will be used at the same time, chosen
	from content 
	m_j is the automata with the smallest number and stop_val the smallest number
	of automata that can be left in content before needing to place it in the current column
	(only used by Algorithm 3, with Algorithm 2 stop_val is always equal to multi, i.e. the last column)
	"""
	if len(content) == 0:
		# terminal case, there are no automata left
		yield []
	else:
		# In the case of Algorithm 3, when we have reached column a_j-1 and m_j hasn't been placed yet
		if mode == ISO and len(content) == stop_val and m_j in content: 
			newcontent = content.copy()
			newcontent.remove(m_j)
			iterator = zip(combinations(newcontent, multi-1), repeat((m_j)))
		
		else:
			iterator = zip(combinations(content, multi), repeat(()))
		
		# Enumerating the possible contents for the current column
		for content_block in iterator:
			
			# Both iterators don't return the same format, so we need to harmonize it
			block = []
			if isinstance(content_block[1], int):
				block += [content_block[1]]
			for i in content_block[0]:
				block += [i]
			
			# Removing the contents from the pool
			newlist = content.copy()			
			for number in block:
				newlist.remove(number)
			
			# Recursive call	
			for arrangement in arrangements_rec(newlist, mode, j, multi, stop_val, m_j):
				yield [block] + arrangement


################################
# main
################################

# init time and counter
print(n)
count = 0          # init counter
timestart = time() # init timer

# iterate over bloc-parallel update modes
for bp in ite(n, mode, output, with_bs):
        # print bp and increment counter
	if output == BOTH:
		print(bp)
	else:
		print(bp[output])
	count += 1
timeend = time() # stop timer

# print time and counter
print('time = {}'.format(timeend-timestart))
print('count = {}'.format(count))

