# blockpargen

Generator of block-parallel update modes, in the context of automata networks of size ``n``.
Block-parallel update modes are dual of block-sequential update modes:
each block-parallel update mode is syntaxically represented as a set of lists, where each integer
from ``0`` to ``n-1`` appears exactly once (a _partitioned order_ of ``n``).
Its semantics is that automata are updated sequentially within each list, and lists are updated in parallel.
The period is the least common multiple of the list sizes.

This code implements the three enumeration algorithms presented in the article
"On countings and enumerations of block-parallel automata networks"
by Kévin Perrot, Sylvain Sené and Léah Tapin, available on
[arxiv:2304.09664](https://arxiv.org/abs/2304.09664).

### Usage

For a given automata network size ``n``, it is possible to generate:

* ``BPn`` (argument ``all``): all block-parallel update modes,
* ``BPn0`` (argument ``eq0``): block-parallel update modes which _may_ give different dynamics
  (depending on the automata network on which it is applied;
  one representative of each class is generated),
* ``BPn*`` (argument ``iso``): block-parallel update modes which _may_ give non-isomorphic limit dynamics
  (depending on the automata network on which it is applied;
  one representative of each class is generated).

```
usage: blockpargen.py [-h] [-f {p_order,seq_blocks,both}] [--BPonly] n {all,eq0,iso}

positional arguments:
  n                     number of automata
  {all,eq0,iso}         update mode: 'all' for BPn, 'eq0' for BPn0, 'iso' for BPn*.

options:
  -h, --help            show this help message and exit
  -f {p_order,seq_blocks,both}, --format {p_order,seq_blocks,both}
                        output format: 'p_order' for partitioned orders, 'seq_blocks' for
                        sequences of blocks, 'both' for partitioned orders and sequences of
                        blocks (in that order).
  --BPonly              generate only update modes that are not block-sequential.
```

### Example

``python3 blockpargen.py 4 all`` outputs the thirteen block-parallel update modes on four automata:

```
[[0], [1], [2]]
[[0], [1, 2]]
[[0], [2, 1]]
[[1], [0, 2]]
[[1], [2, 0]]
[[2], [0, 1]]
[[2], [1, 0]]
[[0, 1, 2]]
[[0, 2, 1]]
[[1, 0, 2]]
[[1, 2, 0]]
[[2, 0, 1]]
[[2, 1, 0]]
```

As an example, the block-parallel update mode ``[[0],[1,2]]`` has two lists ``[0]`` and ``[1,2]``,
meaning that
during the first substep automata ``0`` and ``1`` are updated,
during the second substep automata ``0`` and ``2`` are updated.
This block-parallel update mode has period two,
and corresponds to the sequence of blocks ``({0,1},{0,2})``.

### Main

In the ``main`` (bottom of the file), it is possible to perform any kind of operation
for each block-parallel update mode ``bp`` that is enumerated (each block-parallel is a ``list of list``).

